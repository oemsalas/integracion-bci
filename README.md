# Integracion Bci

Al correr el @SpringBootApplication class IntegracionBciApplication este creara las tablas en memoria, sin datos.

## test unitarios 

Al correr el @SpringBootTest class IntegracionBciApplicationTests este correra 2 test unitarios

## url a la api para crear usuario

localhost:8080/api/create

body en formato json

{
"name": "Juan Rodriguez",
"email": "perez@rodriguez.org",
"password": "hunter2",
"phones": [
{
"number": "1234567",
"citycode": "1",
"countrycode": "57"
}
]
}

## url a la api para listar usuarios
http://localhost:8080/api/list

## URL para utilizar el swagger
http://localhost:8080/swagger-ui/index.html

## URL para chequear las tablas que se crearon en memoria
http://localhost:8080/h2-console/