package examen.integracionbci.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import examen.integracionbci.modelo.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

  Usuario findById(long id);
  
  @Query(value="select * from usuario a where a.email= :email", nativeQuery=true)
  List<Usuario> getUsuarioByEmail(String email);
}
