package examen.integracionbci.mapeo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import examen.integracionbci.json.JsonPhones;
import examen.integracionbci.json.JsonRetorno;
import examen.integracionbci.json.JsonUsuario;
import examen.integracionbci.modelo.Phones;
import examen.integracionbci.modelo.Usuario;

public class MapeoEntity {
	
	public static Usuario entityUsuarioConJsonUsuario(JsonUsuario ju) {
		Usuario user = new Usuario();
		user.setName(ju.getName());
		user.setEmail(ju.getEmail());
		user.setPassword(ju.getPassword());
		
		List<Phones> listPhones=new ArrayList<Phones>();
		
		for (JsonPhones jp : ju.getPhones()) {
			Phones p = new Phones();
			p.setCitycode(jp.getCitycode());
			p.setCountrycode(jp.getCountrycode());
			p.setNumber(jp.getNumber());
			listPhones.add(p);
		}
		user.setPhones(listPhones);
		
		//parametros extra
		user.setCreated(new Date());
		user.setModified(new Date());
		user.setLast_login(new Date());
		user.setIsactive(Boolean.TRUE);
		user.setToken(UUID.randomUUID()+"");	//a mejorar. hacerlo con jwt
		return user;
	}
	
	public static JsonRetorno entityJsonRetornoConUsuario(Usuario user) {
		
		List<JsonPhones> listPhones=new ArrayList<JsonPhones>();
		
		for (Phones jp : user.getPhones()) {
			JsonPhones p = new JsonPhones();
			p.setCitycode(jp.getCitycode());
			p.setCountrycode(jp.getCountrycode());
			p.setNumber(jp.getNumber());
			listPhones.add(p);
		}
		
		JsonRetorno jr = new JsonRetorno(user.getName(),user.getEmail(),user.getPassword(), listPhones);
		jr.setId(user.getId());
		jr.setCreated(user.getCreated());
		jr.setModified(user.getModified());
		jr.setLast_login(user.getLast_login());
		jr.setIsactive(user.isIsactive());
		jr.setToken(user.getToken());
		
		return jr;
	}
}
