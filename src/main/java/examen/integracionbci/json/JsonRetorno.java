package examen.integracionbci.json;

import java.util.Date;
import java.util.List;

public class JsonRetorno extends JsonUsuario {
	
	//parametros extra
	private long id;
	private Date created;
	private Date modified;
	private Date last_login;
	private boolean isactive;
	private String token;
	
	public JsonRetorno(String name, String email, String password, List<JsonPhones> phones) {
		super(name, email, password, phones);
	}


	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}


	public Date getCreated() {
		return created;
	}


	public void setCreated(Date created) {
		this.created = created;
	}


	public Date getModified() {
		return modified;
	}


	public void setModified(Date modified) {
		this.modified = modified;
	}


	public Date getLast_login() {
		return last_login;
	}


	public void setLast_login(Date last_login) {
		this.last_login = last_login;
	}


	public boolean isIsactive() {
		return isactive;
	}


	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}
	
}
