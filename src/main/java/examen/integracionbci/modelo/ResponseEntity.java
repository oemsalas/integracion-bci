package examen.integracionbci.modelo;

/**
 * @author oemsalas
 */
public class ResponseEntity {
	private String compressed;	

	
	public ResponseEntity(){	
	};
	
	public ResponseEntity(String compress) {
		this.compressed=compress;		
	}

	public String getCompressed() {
		return compressed;
	}

	public void setCompressed(String compressed) {
		this.compressed = compressed;
	}
	
}
