package examen.integracionbci.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import examen.integracionbci.json.JsonError;
import examen.integracionbci.json.JsonUsuario;
import examen.integracionbci.mapeo.MapeoEntity;
import examen.integracionbci.modelo.Usuario;
import examen.integracionbci.repository.UsuarioRepository;
import examen.integracionbci.utilidades.Validar;

@CrossOrigin(origins = "http://localhost:8080")
//@EnableAutoConfiguration
@RestController
@RequestMapping("api")
public class ServiceApiController {
	
	@Autowired
	private UsuarioRepository repository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@GetMapping(value="/list")
	public List<Usuario> getUsuarios() {
		List<Usuario> listUsuario = (List<Usuario>) repository.findAll();
		return listUsuario;
	}
	
	/**
	 * Method to save users in the db.
	 * @param ju
	 * @return
	 */
	@PostMapping(value= "/create", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> create(@RequestBody JsonUsuario ju) {
		logger.debug("Saving users. "+ ju);	
		
		if(Validar.passwordDelUsuario(ju.getPassword()) ==Boolean.FALSE) {
			JsonError je0=new JsonError("EL password no valida");
			return new ResponseEntity<Object>(je0, HttpStatus.OK);
		}
			
		if(Validar.emailDelUsuario(ju.getEmail())) {
			if(repository.getUsuarioByEmail(ju.getEmail()).isEmpty()) {
				Usuario user = repository.save(MapeoEntity.entityUsuarioConJsonUsuario(ju));
								
				return new ResponseEntity<Object>(user, HttpStatus.OK);
			}else {
				JsonError je1=new JsonError("El correo ya registrado");
				return new ResponseEntity<Object>(je1, HttpStatus.OK);
			}
		}else {
			JsonError je2=new JsonError("El correo no valida");
			return new ResponseEntity<Object>(je2, HttpStatus.OK);
		}
	}
}