package examen.integracionbci.utilidades;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validar {
	public static boolean emailDelUsuario(String email) {
		 // Patrón para validar el email
        Pattern pattern = Pattern.compile(Constantes.PATRON_EMAIL);
 
        Matcher mather = pattern.matcher(email);
 
        if (mather.find() == true) {
            System.out.println("El email ingresado es válido.");
            return Boolean.TRUE;
        } else {
            System.out.println("El email ingresado es inválido.");
            return Boolean.FALSE;
        }
	}
	
	public static boolean passwordDelUsuario(String pass) {
		// Patrón para validar el password
        Pattern pattern = Pattern.compile(Constantes.PATRON_PASSWORD);
 
        Matcher mather = pattern.matcher(pass);
 
        if (mather.find() == true) {
            System.out.println("El password ingresado es válido.");
            return Boolean.TRUE;
        } else {
            System.out.println("El password ingresado es inválido.");
            return Boolean.FALSE;
        }
	}
	
	public static void main(String[] args) {
		Validar.passwordDelUsuario("#%&/()");
		Validar.passwordDelUsuario("hunter2");
	}
}