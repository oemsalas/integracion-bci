package examen.integracionbci;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import examen.integracionbci.json.JsonPhones;
import examen.integracionbci.json.JsonUsuario;
import examen.integracionbci.mapeo.MapeoEntity;
import examen.integracionbci.modelo.Usuario;
import examen.integracionbci.repository.UsuarioRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class IntegracionBciApplicationTests {
	
	@Autowired
	private UsuarioRepository repository;
	
	@DisplayName("Test Spring repository.save()")
	@Test
	void testCreate() {
		List<JsonPhones> listPhones=new ArrayList<JsonPhones>();
		
		JsonPhones phone=new JsonPhones();
		phone.setCitycode("1");
		phone.setCountrycode("234");
		phone.setNumber("12345");
		listPhones.add(phone);
		JsonUsuario ju=new JsonUsuario("omar", "oemsalas@gmail.com", "123456", listPhones);
		
		Usuario savedUser = repository.save(MapeoEntity.entityUsuarioConJsonUsuario(ju));
		System.out.println(savedUser);
		
		Optional<Usuario> userActual=repository.findById(savedUser.getId());
		assertEquals(savedUser.getName(), userActual.get().getName());
	}
	
	@DisplayName("Test Spring get usuario by email")
	@Test
	void testGetByEmail() {
		List<JsonPhones> listPhones=new ArrayList<JsonPhones>();
		
		JsonPhones phone=new JsonPhones();
		phone.setCitycode("1");
		phone.setCountrycode("234");
		phone.setNumber("12345");
		listPhones.add(phone);
		JsonUsuario ju=new JsonUsuario("perez", "perez@rodriguez.org", "123456", listPhones);
		
		Usuario savedUser = repository.save(MapeoEntity.entityUsuarioConJsonUsuario(ju));
		System.out.println(savedUser);
		
		List<Usuario>userActual=repository.getUsuarioByEmail(savedUser.getEmail());
		assertEquals(savedUser.getEmail(), userActual.get(0).getEmail());
	}

}
